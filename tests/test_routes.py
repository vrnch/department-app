import unittest
import os

from app import app, routes, models
import requests


class RestApiTest(unittest.TestCase):

    def setUp(self):
        app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL')

    def test_get_employees(self):
        response = requests.get('http://127.0.0.1:5000/employees')
        self.assertEqual(response.status_code, 200)

    def test_get_employee(self):
        response = requests.get('http://127.0.0.1:5000/employees/150')
        self.assertEqual(response.status_code, 200)

    def test_get_departments(self):
        response = requests.get('http://127.0.0.1:5000/departments')
        self.assertEqual(response.status_code, 200)

    def test_get_department(self):
        response = requests.get('http://127.0.0.1:5000/departments/1')
        self.assertEqual(response.status_code, 200)

    def test_get_departments_and_avg_salary(self):
        response = requests.get('http://127.0.0.1:5000/departments_and_avg_salary')
        self.assertEqual(response.status_code, 200)

    def test_get_employees_by_date_of_birth(self):
        response = requests.get("http://127.0.0.1:5000/employees/from='2005-01-01'&to='2006-01-01'")
        self.assertEqual(response.status_code, 200)

    def test_create_department(self):
        department = models.Department(name='unittest')
        response = requests.post('http://127.0.0.1:5000/departments', data=department.to_dict())
        self.assertEqual(response.status_code, 200)

    def test_create_employee(self):
        employee = models.Employee(
            first_name='unittest',
            last_name='unittest',
            department_id=1,
            date_of_birth='2000-01-01',
            salary=228
        )
        response = requests.post('http://127.0.0.1:5000/employees', json=employee.to_dict())
        self.assertEqual(response.status_code, 200)

    def test_update_department(self):
        data = {"name": "unittest111111"}
        response = requests.put('http://127.0.0.1:5000/departments/13', json=data)
        self.assertEqual(response.status_code, 200)

    def test_update_employee(self):
        data = {
            "first_name": "unittest111111111111"
        }
        response = requests.put("http://127.0.0.1:5000/employees/203", json=data)
        self.assertEqual(response.status_code, 200)

    def test_delete_department(self):
        response = requests.delete("http://127.0.0.1:5000/departments/13")
        self.assertEqual(response.status_code, 200)

    def test_delete_employee(self):
        response = requests.delete("http://127.0.0.1:5000/employees/206")
        self.assertEqual(response.status_code, 200)
