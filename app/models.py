from app import db


class Department(db.Model):
    """
    A class to represent the `department` table

    Attributes:
        id (int, Optional): ID of the department.
        name (str): Name of the department e.
        employees (Employee, optional): One-to-many relationship with the `employee` table in the database.

    Methods:
        to_dict(): Converts an instance of the Department class into dictionary.
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    employees = db.relationship('Employee', backref='department', lazy=True)

    def __repr__(self):
        return f'<Department {self.name}>'

    def to_dict(self):
        """
        Converts an instance of the Department class into dictionary.

        Returns:
            Dictionary representation of the instance.
        """
        data = {
            'id': self.id,
            'name': self.name,
        }
        return data

    def from_dict(self, data):
        """
        Updates the instance of the Department class with the information from the dictionary.

        Parameters:
            data (dict): The dictionary that matches the Department class attributes.
        """
        for field in ['name']:
            if field in data:
                setattr(self, field, data[field])


class Employee(db.Model):
    """
    A class to represent the `employee` table in the database.

    Attributes:
        id (int, Optional): ID of the employee.
        first_name (str): First name of the employee.
        last_name (str): Last name of the employee.
        department_id (int): Foreign key to the `department` table in the database.
        date_of_birth (date): Date of birth of the employee.
        salary (int): Salary of the employee.
    """
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    department_id = db.Column(db.Integer, db.ForeignKey('department.id'))
    date_of_birth = db.Column(db.Date)
    salary = db.Column(db.Numeric)

    def __repr__(self):
        return f'<Employee {self.first_name} {self.last_name}>'

    def to_dict(self):
        """
        Converts an instance of the Employee class into dictionary.

        Returns:
            Dictionary representation of the instance.
        """
        data = {
            'id': self.id,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'department_id': self.department_id,
            'date_of_birth': self.date_of_birth,
            'salary': int(self.salary),
        }
        return data

    def from_dict(self, data):
        """
        Updates the instance of the Employee class with the information from the dictionary.

        Parameters:
            data (dict): The dictionary that matches the Employee class attributes.
        """
        for field in ['first_name', 'last_name', 'department_id', 'date_of_birth', 'salary']:
            if field in data:
                setattr(self, field, data[field])
