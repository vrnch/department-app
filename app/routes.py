from app import app, db
from app.forms import SearchEmployeeForm
from app.models import Department, Employee
from flask import jsonify, request, render_template, url_for
import webbrowser


@app.route('/', methods=['GET'])
def index():
    """
    Renders a basic menu functions in the html-file.
    """
    return render_template('base.html')


@app.route('/search_employees_by_date_of_birth', methods=['GET', 'POST'])
def search_employees_by_date_of_birth():
    """
    Renders a form to search employees by date of birth. Opens it in the new tab.
    """
    form = SearchEmployeeForm()
    if form.is_submitted():
        from_date = form.from_date.data
        to_date = form.to_date.data

        webbrowser.open_new_tab(request.host + url_for("show_employees_by_date_of_birth", from_=from_date, to=to_date))

    return render_template('search_employee_by_date_of_birth.html', form=form)


@app.route("/show_employees_by_date_of_birth/from='<string:from_>'&to='<string:to>'", methods=['GET'])
def show_employees_by_date_of_birth(from_, to):
    """
    Renders a table containing employees found by date of birth.

    Parameters:
        from_ (date): The date on which the search has to be started.
        to (date): The date on which the search has to be stopped.
    """
    query = f"""select
        e.first_name
        ,e.last_name
        ,d.name
        ,e.salary
        ,e.date_of_birth
    from
        employee e
    join
        department d
    on 
        e.department_id = d.id
    where 
        e.date_of_birth between '{from_}' and '{to}'
    order by 
        e.date_of_birth;"""
    result = db.engine.execute(query)

    return render_template('show_employees_by_date_of_birth.html', result=result)


@app.route('/show_departments_and_avg_salary', methods=['GET'])
def show_departments_and_avg_salary():
    """
    Renders a table containing average salary distribution by departments.
    """
    query = """select
        d.id
         ,d.name
         ,avg(e.salary) avg_salary
    from
        department d
    join
        employee e on d.id = e.department_id
    group by
        d.id, d.name
    order by
        avg_salary;"""

    result = db.engine.execute(query)

    return render_template('departments_and_avg_salary.html', result=result)


@app.route('/departments_and_avg_salary', methods=['GET'])
def get_departments_and_avg_salary():
    """
    Creates a json containing average salary distribution by departments.

    Returns:
         The json with a list of average salary distribution by departments.
    """
    query = """select
        d.id
         ,d.name
         ,avg(e.salary) avg_salary
    from
        department d
    join
        employee e on d.id = e.department_id
    group by
        d.id, d.name
    order by
        avg_salary;"""
    result = db.engine.execute(query)
    result = [
        {
            'id': id_,
            'name': name,
            'avg_salary': int(avg_salary),
        } for id_, name, avg_salary in result
    ]
    return jsonify(result)


@app.route("/employees/from=<string:from_>&to=<string:to>", methods=['GET'])
def get_employees_by_date_of_birth(from_, to):
    """
    Creates a json containing employees found by date of birth.

    Parameters:
        from_ (date): The date on which the search has to be started.
        to (date): The date on which the search has to be stopped.

    Returns:
         The json with a list of employees found by date of birth.
    """

    query = f"""select
    e.first_name
    ,e.last_name
    ,d.name
    ,e.salary
    ,e.date_of_birth
    from
        employee e
    join
        department d
    on 
        e.department_id = d.id
    where 
        e.date_of_birth between {from_} and {to}
    order by 
        e.date_of_birth;"""
    result = db.engine.execute(query)

    result = [
        {
            'first_name': first_name,
            'last_name': last_name,
            'department_name': department_name,
            'salary': int(salary),
            'date_of_birth': date_of_birth,
        } for first_name, last_name, department_name, salary, date_of_birth in result
    ]

    return jsonify(result)


@app.route('/departments', methods=['GET'])
def get_departments():
    """
    Creates a json containing a list of departments.

    Returns:
        The json with a list of departments.
    """
    departments = Department.query.order_by(Department.id.desc()).all()
    return jsonify([d.to_dict() for d in departments])


@app.route('/employees', methods=['GET'])
def get_employees():
    """
    Creates a json containing a list of employees.

    Returns:
        The json with a list of employees.
    """
    employees = Employee.query.order_by(Employee.id.desc()).all()
    return jsonify([e.to_dict() for e in employees])


@app.route('/departments/<int:id_>', methods=['GET'])
def get_department(id_):
    """
    Creates a json containing a department found by ID.

    Parameters:
        id_ (int): ID of the department.

    Returns:
        The json with the department.
    """
    department = Department.query.get_or_404(id_)
    return jsonify(department.to_dict())


@app.route('/employees/<int:id_>', methods=['GET'])
def get_employee(id_):
    """
    Creates a json containing an employee found by ID.

    Parameters:
        id_ (int): ID of the employee.

    Returns:
        The json with the employee.
    """
    employee = Employee.query.get_or_404(id_)
    return jsonify(employee.to_dict())


@app.route('/departments', methods=['POST'])
def create_department():
    """
    Adds a new department record to the database.

    Returns:
        The json with the new department.
    """
    data = request.json
    department = Department(name=data['name'])
    db.session.add(department)
    db.session.commit()
    return jsonify(department.to_dict())


@app.route('/employees', methods=['POST'])
def create_employee():
    """
    Adds a new employee record to the database.

    Returns:
        The json with the new employee.
    """
    data = request.json
    employee = Employee(
        first_name=data['first_name'],
        last_name=data['last_name'],
        department_id=data['department_id'],
        date_of_birth=data['date_of_birth'],
        salary=data['salary']
    )
    db.session.add(employee)
    db.session.commit()
    return jsonify(employee.to_dict())


@app.route('/departments/<int:id_>', methods=['PUT'])
def update_department(id_):
    """
    Updates a department database record found by its ID.

    Parameters:
        id_ (int): ID of the department.

    Returns:
        The json with the updated department.
    """
    department = Department.query.get_or_404(id_)
    data = request.json
    department.from_dict(data)
    db.session.commit()
    return jsonify(department.to_dict())


@app.route('/employees/<int:id_>', methods=['PUT'])
def update_employee(id_):
    """
    Updates an employee database record found by its ID.

    Parameters:
         id_ (int): ID of the employee.

    Returns:
        The json with the updated employee.
    """
    employee = Employee.query.get_or_404(id_)
    data = request.json
    employee.from_dict(data)
    db.session.commit()
    return jsonify(employee.to_dict())


@app.route('/departments/<int:id_>', methods=['DELETE'])
def delete_department(id_):
    """
    Deletes the department database record found by its ID.

    Parameters:
        id_ (int): ID of the department.

    Returns:
        The json with the deleted department.
    """
    department = Department.query.get_or_404(id_)
    db.session.delete(department)
    db.session.commit()
    return jsonify(department.to_dict())


@app.route('/employees/<int:id_>', methods=['DELETE'])
def delete_employee(id_):
    """
    Deletes the employee database record found by its ID.

    Parameters:
        id_ (int): ID of the employee.

    Returns:
        The json with the deleted employee.
    """
    employee = Employee.query.get_or_404(id_)
    db.session.delete(employee)
    db.session.commit()
    return jsonify(employee.to_dict())
