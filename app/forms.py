from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class SearchEmployeeForm(FlaskForm):
    """
    A form to provide an opportunity for searching employees by date of birth.

    Attributes:
        from_date (StringField): The date on which the search has to be started.
        to_date (StringField): The date on which the search has to be stopped.
        submit (SubmitField): Submit button.
    """
    from_date = StringField('From', validators=[DataRequired()])
    to_date = StringField('To', validators=[DataRequired()])
    submit = SubmitField('Search')
