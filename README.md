# department-app
My RD External Python Course Graduation Work at [[RD UA Py] Online Python External Program #1](https://www.training.epam.ua/#!/Training/2583?lang=en).  
This is a web application for managing departments and employees.

# Getting Started

### Prerequisites

1. Ubuntu 20.10
2. Python 3.8+, `python3-dev`, `venv`, `pip`
3. MySQL

### Installing

1. Update and upgrade your system:
```bash
sudo apt update
sudo apt upgrade
```

2. Check the installed Python 3 version:
```bash
python3 -V
```

3. Install `pip`:
```bash
sudo apt install -y python3-pip
```

4. Install additional tools:
```bash
sudo apt install python3-dev build-essential libssl-dev libffi-dev 
```

5. Install `venv`:
```bash
sudo apt install -y python3-venv
```

6. Install MySQL:
```bash
sudo apt install mysql-server mysql-client
```
7. Check if MySQL have installed:
```bash
mysql -V
```

8. Start MySQL:
```bash
sudo service mysql start
```

9. Connect to MySQL as root:
```bash
sudo mysql -u root
```

10. Create database, user, and grant user privileges:
```sql
create database department_app;
create user 'department_app'@'localhost' identified by 'admin';
grant all privileges on department_app.* to 'department_app'@'localhost';
```

If you want to change the username, password or database name, please change the `SQLALCHEMY_DATABASE_URI` variable in the `config.py` file as well.  
The structure of the MySQL URL is the following:
```python
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://<username>:<db-password>@localhost:3306/<db-name>'
```

11. Install `git`:
```bash
sudo apt install git
```

12. Create a directory that will contain the repository, where `<dir-name>` is the name of the directory, and change the current directory to the created directory:
```bash
mkdir <dir-name>
cd <dir-name>
```

13. Clone the repository:
```bash
git clone https://gitlab.com/vrnch/department-app.git
```

14. Create and activate a virtual environment inside the `department_app` directory (`<env-name>` is the name of your virtual environment):
```bash
cd department_app
python3 -m venv <env-name>
source <env-name>/bin/activate 
```

15. Install the required packages within the created virtual environment:
```bash
pip install -r requirements.txt
```

16. Import the application:
```bash
export FLASK_APP=department_app.py
```

17. Create tables in your database:
```bash
flask db init
flask db migrate
flask db upgrade
```

18. Start `department-app` under Gunicorn:
```bash
gunicorn -b localhost:5000 -w 4 department_app:app
```


### API
You can test the REST API using `httpie` package that has been installed in the step #15.  

| Description | Verb | URN | `httpie` command | Note |
| :--- | :---: | :---: | :--- | --- |
| Get average salary distribution by departments as a list | GET | `/departments_and_avg_salary` | `http GET :5000/departments_and_avg_salary` |
| Get a list of the all employees found by date of birth | GET | `/employees/from=<from>&to=<to>` | `http GET :5000/employees/from=<from>&to=<to>` | The `from` and `to` parameters must be presented in the `yyyy-mm-dd` format and in the string type |
| Get a list of the all employees | GET | `/employees` | `http GET :5000/employees` |
| Get an employee | GET | `/employees/<id>` | `http GET :5000/employees/<id>` | The `id` parameter must be an integer. | 
| Create an employee | POST | `/employees` | `http POST :5000/employees date_of_birth='<date_of_birth>' department_id=<department_id> first_name='<first_name>' last_name='<last_name>' salary=<salary>` | `date_of_birth` is a string in the `yyyy-mm-dd` format;<br>`department_id` and `salary` are integers;<br>`first_name` and `last_name` are strings. |
| Update an employee | PUT | `/employees/<id>` | `http PUT :5000/employees/<id> <fields>` | The `fields` that you can update are:<br> `date_of_birth`<br> `department_id`<br> `first_name`<br> `last_name`<br>`salary`
| Delete an employee | DELETE | `/employees/<id>` | `http DELETE ` |
| Get a list of the all departments | GET | `/departments` | `http GET :5000/departments` |
| Get a department | GET | `/departments/<id>` | `http GET :5000/departments/<id>` |
| Create a department | POST | `/departments` | `http POST :5000/departments name=<department-name>` |
| Update a department | PUT | `/departments/<id>` | `http PUT :5000/departments/<id> name=<new-department-name>` |
| Delete a department | DELETE | `/department/<id>` | `http DELETE :5000/departments/<id>` |

