# Table of Contents
- [1. Introduction](#1-introduction)
  * [1.1 Purpose](#11-purpose)
  * [1.2 Document Conventions](#12-document-conventions)
  * [1.3 Intended Audience and Reading Suggestions](#13-intended-audience-and-reading-suggestions)
  * [1.4 Project Scope](#14-project-scope)
- [2. Overall Description](#2-overall-description)
  * [2.1 Product Perspective](#21-product-perspective)
  * [2.2 Product Features](#22-product-features)
  * [2.3 Functionality](#23-functionality)
  * [2.4 Operating Environment](#24-operating-environment)
- [3. System Features](#3-system-features)
  * [3.1 Clent/Server System](#31-clent-server-system)
  * [3.2 Hardware System](#32-hardware-system)
  * [3.3 Software System](#33-software-system)

# 1. Introduction
## 1.1 Purpose
The purpose of this document is to build a web application for managing departments and employees.

## 1.2 Document Conventions
This document uses the following conventions:  

|  |  |
| :--- | :--- |
| DB | Database |
| Table | Database table |
| CRUD | Create, read, update and delete operations on a resource |
| ER | Entity-relationship |

## 1.3 Intended Audience and Reading Suggestions
This project is a prototype for the department and employee management system. This has been implemented under the guidance of lecturers and the Flask Mega-Tutorial.

## 1.4 Project Scope  
The purpose of the the department and employee management system is to easy staff management system at companies and create a convenient and easy-to-use application for managers. The system is based on a relational database and provides CRUD operations on departments and employees.


# 2. Overall Description
## 2.1 Product Perspective
An employee and department DB system stores the following information:
* **Department details**:  
It includes departments' names.

* **Employee details:**  
It includes information about employees: name, date of birth, salary, department's ID.


## 2.2 Product Features
The major features of employee and department DB system as shown in below ER model.  
![alt](epam-external-interview-proj.png)
## 2.3 Functionality 
The application allows to:
* display a list of departments and the average salary (calculated automatically) for these departments;
* display a list of employees in the departments with an indication of the salary for each employee and a search field to search for employees born on a certain date or in the period between dates;
* change (add / edit / delete) the above data.

## 2.4 Operating Environment
Operating environment for the department and employee management system is as listed below.  

| | |
| --- | --- |
| OS | Linux |
| DBMS | MySQL |
| Programming Language | Python v3.8+ |
| Framework | Flask |
| DB ORM | SQLAlchemy | 

# 3. System Features
## 3.1 Clent/Server System
* **Front-end**: html
* **Back-end**: Python, MySQL, Jinja

## 3.2 Hardware System
* Linux server
* A browser with HTML support

## 3.3 Software System
* For CRUD operations: `httpie`, Postman or Linux terminal
* For graphical interface usage: a web browser