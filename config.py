import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://department_app:admin@localhost:3306/department_app'  #os.environ.get('DATABASE_URL') #or 'sqlite:///' + os.path.join(basedir, 'app.db')
    MY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_TRACK_MODIFICATIONS = True
